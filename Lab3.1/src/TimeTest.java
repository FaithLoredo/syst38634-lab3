import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
//test
class TimeTest {

	@Test
	final void testGetTotalSeconds() {
		assertTrue(Time.getTotalSeconds("12:05:99")!=43505);
	}

	@Test
	final void testGetSeconds() {
		assertTrue(Time.getSeconds("12:02:50")==50); // TODO
	}

	@Test
	final void testGetTotalMinutes() {
		assertTrue(Time.getTotalMinutes("01:60:00")==60); // TODO
	}

	@Test
	final void testGetTotalHours() {
		assertTrue(Time.getTotalHours("24:00:00")==24); // TODO
	}

}
